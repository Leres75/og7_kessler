package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;
	private static long[] area;
	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		area = list;
		return interpolation(0, list.length-1, searchValue);
	}
	
	public static int interpolation(int von, int bis, long x){
		if(area[von] > x || area[bis] < x){
			return -1;
		}
		if(von<bis){
			//interpolieren
			int marker = (
					von + (int)((double)(x - area[von])
					/ (area[bis] - area[von])
					* (bis - von))
					);
			//gesuchte zahl gefunden?
			if(area[marker] == x) return marker;
			//links weitersuchen
			if(area[marker] > x)
				return interpolation(von, marker-1, x);
			//rechts weitersuchen
			else return interpolation(marker+1, bis, x);
		}
		return NOT_FOUND;
	}


}

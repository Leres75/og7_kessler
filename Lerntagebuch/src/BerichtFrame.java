
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class BerichtFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public BerichtFrame(MeineSuperDuperGUI gui) {

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		JLabel label = new JLabel("Bericht:");
		label.setFont(new Font("Monospaced", Font.BOLD, 32));
		contentPane.add(label, BorderLayout.NORTH);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setText(gui.getLogic().createReport());
		label.setFont(new Font(label.getFont().getFontName(), Font.PLAIN, 26));
		contentPane.add(textArea, BorderLayout.CENTER);
	}

}


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

public class MeineSuperDuperGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	private Logic logic;
	private JTextArea textAreaAllEntries;
	public Logic getLogic() {
		return logic;
	}

	public void setLogic(Logic logic) {
		this.logic = logic;
	}

	private JButton btnNeuerEintrag = new JButton("Neuer Eintrag...");
	private JButton btnBericht = new JButton("Bericht...");
	private JButton btnBeenden = new JButton("Beenden");
	private JButton btnBack = new JButton("Zur�ck");
	private JScrollPane scrollPane = new JScrollPane();
	private JLabel lblLerntagebuchVon;
	private JPanel panel = new JPanel();
	private JPanel panel2 = new JPanel();
	private MeineSuperDuperGUI thisGUI = this;

	public MeineSuperDuperGUI() {
		setTitle("Mein Lerntagebuch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 350);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		logic = new Logic(new FileControl(new File("miriam.dat")));

		lblLerntagebuchVon = new JLabel("Lerntagebuch von " + logic.getLearnerName());
		lblLerntagebuchVon.setFont(new Font("Tahoma", Font.PLAIN, 21));
		contentPane.add(lblLerntagebuchVon, BorderLayout.NORTH);

		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		textAreaAllEntries = new JTextArea();
		textAreaAllEntries.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textAreaAllEntries.setEditable(false);
		updateTextArea();

		scrollPane.setViewportView(textAreaAllEntries);

		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(7);
		flowLayout.setHgap(10);
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);


		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contentPane.remove(panel2);
				panel.remove(btnBack);
				contentPane.add(scrollPane, BorderLayout.CENTER);
				revalidate();
				repaint();
			}
		});
		
		
		btnNeuerEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.remove(scrollPane);
				panel2.setLayout(null);
				panel.add(btnBack, FlowLayout.LEFT);
				JTextField TextFieldDate = new JTextField("Datum");
				TextFieldDate.setBounds(5, 5, 300, 20);
				JTextField TextFieldFach = new JTextField("Fach");
				TextFieldFach.setBounds(5, 35, 300, 20);
				JTextField TextFieldBeschreibung = new JTextField("Beschreibung");
				TextFieldBeschreibung.setBounds(5, 65, 300, 20);
				JTextField TextFieldDauer = new JTextField("Dauer");
				TextFieldDauer.setBounds(5, 95, 300, 20);
				JButton btnSpeichern = new JButton("Speichern");
				
				btnSpeichern.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						boolean parseable = true;
						try {
							Lerneintrag le = new Lerneintrag();
							le.setBeschreibung(TextFieldBeschreibung.getText());
							le.setDatum(new SimpleDateFormat("dd.MM.yyyy").parse(TextFieldDate.getText()));
							le.setDauer(Integer.valueOf(TextFieldDauer.getText()));
							le.setFach(TextFieldFach.getText());
							logic.SaveEntryToDisk(le);
							updateTextArea();
						} catch (ParseException e1) {
							e1.printStackTrace();
							parseable = false;
							JOptionPane.showMessageDialog(null, "Could not Parse date, please use: dd.mm.yyyy", "ERROR",
									JOptionPane.OK_OPTION);
						}
						if (parseable) {
							contentPane.remove(panel2);
							contentPane.add(scrollPane, BorderLayout.CENTER);
							revalidate();
							repaint();
						}
					}
				});
				btnSpeichern.setBounds(5, 125, 200, 30);
				panel2.add(TextFieldBeschreibung);
				panel2.add(TextFieldDauer);
				panel2.add(TextFieldDate);
				panel2.add(TextFieldFach);
				panel2.add(btnSpeichern);
				contentPane.add(panel2, BorderLayout.CENTER);
				revalidate();
				repaint();

			}
		});
		panel.add(btnNeuerEintrag);

		panel.add(btnBericht);
		btnBericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							BerichtFrame frame = new BerichtFrame(thisGUI);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		panel.add(btnBeenden);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	public void updateTextArea() {
		String text = "Datum      | Fach         | Aktivit�t                                | Dauer   \n"
				+ "-------------------------------------------------------------------------------\n";

		List<Lerneintrag> eintraege = logic.getListEntries();

		for (Lerneintrag eintrag : eintraege) {
			String temp = "";

			SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy");
			temp += f.format(eintrag.getDatum()) + " | ";

			temp += eintrag.getFach();
			for (; temp.length() < 26;)
				temp += " ";
			temp += "| ";

			temp += eintrag.getBeschreibung();
			for (; temp.length() < 69;)
				temp += " ";
			temp += "| ";

			temp += eintrag.getDauer() + "\n";
			text += temp;
		}
		textAreaAllEntries.setText(text);
	}

}

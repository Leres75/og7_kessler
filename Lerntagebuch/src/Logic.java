import java.util.List;

public class Logic {
	private List<Lerneintrag> ListEntries;
	private String LearnerName;
	private FileControl fc;

	public Logic(FileControl fc) {
		this.fc = fc;
		this.LearnerName = fc.learnerNameEinlesen();
		this.ListEntries = fc.lerneinträgeEinlesen();
	}

	public String createReport() {
		int gesamtDauer = 0;
		for (Lerneintrag lerneintrag : ListEntries)
			gesamtDauer += lerneintrag.getDauer();

		int durchschnitt = gesamtDauer / ListEntries.size();
		String output = "Gesamte Lerneinheiten | Gesamte Lerzeit | Durchschnittiche Lernzeit\n";
		String temp = " " + ListEntries.size() + " Einheiten";
		for (; temp.length() < 35;) {
			temp += " ";
		}
		temp += "| " + gesamtDauer + " Minuten";
		for (; temp.length() < 58;) {
			temp += " ";
		}
		temp += "| " + durchschnitt + " Minuten";
		output += temp;
		return output;
	}

	public List<Lerneintrag> getListEntries() {
		return ListEntries;
	}

	public void setListEntries(List<Lerneintrag> listEntries) {
		ListEntries = listEntries;
	}

	public String getLearnerName() {
		return LearnerName;
	}

	public void setLearnerName(String learnerName) {
		LearnerName = learnerName;
	}

	public void reloadFromDisk() {
		this.LearnerName = fc.learnerNameEinlesen();
		this.ListEntries = fc.lerneinträgeEinlesen();
	}

	public void SaveEntryToDisk(Lerneintrag le) {
		fc.lernEintragSpeichern(le);
		reloadFromDisk();
	}
}

import java.util.Date;

public class Lerneintrag {
	private Date datum;
	private String fach;
	private String beschreibung;
	private int dauer;

	public Lerneintrag(Date datum, String fach, String beschreibung, int dauer) {
		super();
		this.datum = datum;
		this.fach = fach;
		this.beschreibung = beschreibung;
		this.dauer = dauer;
	}

	public Lerneintrag() {
		// TODO Auto-generated constructor stub
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	@Override
	public String toString() {
		return "Lerneintrag [datum=" + datum + ", fach=" + fach + ", beschreibung=" + beschreibung + ", dauer=" + dauer
				+ "]";
	}

}

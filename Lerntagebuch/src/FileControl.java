import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class FileControl {
	private File file;
	public FileControl(File file) {
		this.file = file;
	}
	public String learnerNameEinlesen()  {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String output = br.readLine();
			br.close();
			return output;
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		}
		return null;
	}
	public List<Lerneintrag> lerneinträgeEinlesen() {
		List<Lerneintrag> eintraege;
		eintraege = new ArrayList<Lerneintrag>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			Lerneintrag temp;;
			String st;
			br.readLine();
			while((st = br.readLine()) != null) {
				temp = new Lerneintrag();
				temp.setDatum(new SimpleDateFormat("dd.MM.yyyy").parse(st));
				temp.setFach(br.readLine());
				temp.setBeschreibung(br.readLine());
				temp.setDauer(Integer.valueOf(br.readLine()));
				eintraege.add(temp);
			}
			br.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		}
		
		return eintraege;
	}
	public void lernEintragSpeichern(Lerneintrag le) {
		try {
			FileWriter fileWriter = new FileWriter(this.file, true);
			fileWriter.write("\n" + new SimpleDateFormat("dd.MM.yyyy").format(le.getDatum() ) );
			fileWriter.write("\n" + le.getFach());
			fileWriter.write("\n" + le.getBeschreibung());
			fileWriter.write("\n" + le.getDauer());
		    fileWriter.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.OK_OPTION);
			e.printStackTrace();
		}
		
	}
}

package addons;

public class Addon {
	private int id;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestand;
	private int maxBestand;
	final double MINPREIS = 0.29;
	final double MAXPREIS = 2.99;
	
	//Konstruktor
	public Addon(int id, String bezeichnung, double verkaufspreis, int bestand, int maxBestand) {
		super();
		this.id = id;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.bestand = bestand;
		this.maxBestand = maxBestand;
	}
	//getter und setter
	public double getGesamtwert(){
		return this.bestand * this.verkaufspreis;
	}
	@Override
	public String toString() {
		return "Addon [id=" + id + ", bezeichnung=" + bezeichnung + ", verkaufspreis=" + verkaufspreis + ", bestand="
				+ bestand + ", maxBestand=" + maxBestand + ", MINPREIS=" + MINPREIS + ", MAXPREIS=" + MAXPREIS + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getVerkaufspreis() {
		return verkaufspreis;
	}
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	public int getBestand() {
		return bestand;
	}
	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	public int getMaxBestand() {
		return maxBestand;
	}
	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
}


public class Mannschaftsleiter extends Spieler{
	private String mannschaftsname;
	private int rabatt;
	public Mannschaftsleiter() {
		super();
	}
	public Mannschaftsleiter(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			int trikotnummer, String spielposition, String mannschaftsname, int rabatt) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt, trikotnummer, spielposition);
		this.mannschaftsname = mannschaftsname;
		this.rabatt = rabatt;
	}
	public String getMannschaftsname() {
		return mannschaftsname;
	}
	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
}

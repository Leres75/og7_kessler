
public class Trainer extends Person{
	private char lizensklasse;
	private double gehalt;
	
	
	public Trainer() {
		super();
	}

	public Trainer(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			char lizensklasse, double gehalt) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt);
		this.lizensklasse = lizensklasse;
		this.gehalt = gehalt;
	}
	
	public char getLizensklasse() {
		return lizensklasse;
	}
	public void setLizensklasse(char lizensklasse) {
		this.lizensklasse = lizensklasse;
	}
	public double getGehalt() {
		return gehalt;
	}
	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}
}


public class Schiedsrichter extends Person{
	private int gepfiffeneSpiele;

	public Schiedsrichter() {
		super();
	}

	public Schiedsrichter(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			int gepfiffeneSpiele) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
}

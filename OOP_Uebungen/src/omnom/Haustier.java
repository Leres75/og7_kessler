package omnom;

public class Haustier {
	private int hunger = 100;
	private int muede = 100;
	private int zufrieden = 100;
	private int gesund = 100;
	private String name;

	public Haustier(String name) {

		this.name = name;
	}

	public void fuettern(int i) {
		setHunger(getHunger() +i);
	}

	public void schlafen(int i) {
		setMuede(getMuede() + i);
	}

	public void spielen(int i) {
		setZufrieden(getZufrieden() + i);
	}

	public void heilen() {
		setGesund(100);
	}

	// getters and Setters
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger > 0 && hunger < 100) {
			this.hunger = hunger;
		} else if (hunger < 0) {
			this.hunger = 0;
		}else if(hunger>100){
			this.hunger = 100;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede > 0 && muede < 100) {
			this.muede = muede;
		} else if (muede < 0) {
			this.muede = 0;
		}else if(muede>100){
			this.muede = 100;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden > 0 && zufrieden < 100) {
			this.zufrieden = zufrieden;
		} else if (zufrieden < 0) {
			this.zufrieden = 0;
		}else if(zufrieden>100){
			this.zufrieden = 100;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		this.gesund = gesund;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package zahlensortierer;

import java.util.*;

//TODO add your name as author
/**
 * @author "your name"
 *
 */
public class Zahlensortierer {

	//TODO  
	// vervollst�ndigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer 
	// einliest und die kleinste und gr��te Zahl ausgibt.
	public static void main(String[] args) {
		

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Wieviele Zahlen sollen sortiert werden?");
		int[] zahlen = new int[myScanner.nextInt()];
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print((i+1) + ". Zahl: ");
			zahlen[i] = myScanner.nextInt();
		}
		myScanner.close();
		
		Arrays.sort(zahlen);
		
		System.out.println("zahlen absteigend nach gr��e sortiert:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println((i+1) + ". " + zahlen[zahlen.length - i - 1]);
		}
	}
}

package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;
	private double maxFuellmenge;

	public double getMaxFuellmenge() {
		return maxFuellmenge;
	}

	public void setMaxFuellmenge(double maxFuellmenge) {
		this.maxFuellmenge = maxFuellmenge;
	}

	public Tank(double fuellstand, double maxFuellmenge) {
		this.fuellstand = fuellstand;
		this.maxFuellmenge = maxFuellmenge;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}

}

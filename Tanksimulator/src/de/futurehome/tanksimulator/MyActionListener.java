package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if(fuellstand + 5 <= f.myTank.getMaxFuellmenge()) {fuellstand = fuellstand + 5;}else {fuellstand = f.myTank.getMaxFuellmenge();}
			 
			 f.myTank.setFuellstand(fuellstand);

			 f.lblFuellstand.setText(""+ fuellstand + "Liter " + (fuellstand/f.myTank.getMaxFuellmenge() * 100) + "%");
			 
		}
		
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if(fuellstand - 2 >= 0) {
			fuellstand -= 2;
			f.myTank.setFuellstand(fuellstand);
			
			f.lblFuellstand.setText(""+ fuellstand + "Liter " + (fuellstand/f.myTank.getMaxFuellmenge() * 100) + "%");
			}
		}
		
		if (obj == f.btnZuruecksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			
			f.lblFuellstand.setText("                      ");
		}
		
	}
}